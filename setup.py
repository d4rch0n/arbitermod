import os
from setuptools import setup

# arbitermod
# Auto-moderates a subreddit (reddit.com forum) based on heuristics of user 
# behavior.

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    entry_points = {
        'console_scripts': [
            'arbitermod = arbitermod:main',
            'arbitermod-config = arbitermod.config:main',
        ],
    },
    name = "arbitermod",
    version = "0.0.1",
    description = "Auto-moderates a subreddit (reddit.com forum) based on "
        "heuristics of user behavior.",
    author = "d4rch0n",
    author_email = "d4rch0n@gmail.com",
    license = "GPLv3+",
    keywords = "reddit",
    url = "https://www.bitbucket.org/d4rch0n/arbitermod",
    packages=['arbitermod'],
    package_dir={'arbitermod': 'arbitermod'},
    long_description=read('README.md'),
    classifiers=[
        #'Development Status :: 1 - Planning',
        #'Development Status :: 2 - Pre-Alpha',
        'Development Status :: 3 - Alpha',
        #'Development Status :: 4 - Beta',
        #'Development Status :: 5 - Production/Stable',
        #'Development Status :: 6 - Mature',
        #'Development Status :: 7 - Inactive',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Environment :: Console',
        'Environment :: X11 Applications :: Qt',
        'Environment :: MacOS X',
        'Environment :: Win32 (MS Windows)',
        'Operating System :: POSIX',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python',
    ],
    install_requires=[
        'requests>=2.7.0', 'pyyaml', 'yamlcfg', 'praw', 'nltk', 'numpy',
        'textblob',
    ],
    package_data = {
        'arbitermod': ['*.yml'],
    },
    include_package_data = True,
)
