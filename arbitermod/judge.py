#!/usr/bin/env python

import math
import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import wordnet as wn
from nltk.stem import WordNetLemmatizer
from textblob import TextBlob

from analyzer import text_synset_similarity

N_SENTENCE_BASE = 4
TOPIC_THRESHOLD = 0.15
POLAR_THRESHOLD = 0.0
WAY_OFF_TOPIC_THRESHOLD = 0.04

def topic_score(comment, ss_ctr):
    sims = []
    for synset, ct in ss_ctr:
        similarity = text_synset_similarity(comment.body, synset)
        #print('text to {} is {}'.format(str(synset), similarity))
        sims += [similarity]
    sims = sorted(sims, reverse=True)[:5]
    if len(sims) == 0:
        return None
    avg = sum(sims) / float(len(sims))
    return avg

def polarity_score(comment):
    blob = TextBlob(comment.body)
    polars = []
    for sent in blob.sentences:
        polars += [sent.sentiment.polarity]
    if len(polars) == 0:
        return None
    avg = sum(polars) / float(len(polars))
    return avg

def score_sentence_len(comment):
    ''' Scores comment based on number of sentences.
    Uses logarithmic curve, and guessing N_SENTENCE_BASE is a well-rounded 
    comment length.
    '''
    n = len(list(TextBlob(comment.body).sentences))
    if n == 0:
        return 0.0
    return math.log(n, N_SENTENCE_BASE)

def scores_fail(topic, polar, num,
        topic_threshold=None,
        polar_threshold=None,
        way_off=None):
    ''' Decide whether the scores are good enough. 
    *** Ignoring number of sentences for now ***
    '''
    if topic_threshold is None:
        topic_threshold = TOPIC_THRESHOLD
    if polar_threshold is None:
        polar_threshold = POLAR_THRESHOLD
    if way_off is None:
        way_off = WAY_OFF_TOPIC_THRESHOLD
    return (
        (topic <= topic_threshold and polar <= polar_threshold) or 
        topic <= way_off
    )
