#!/usr/bin/env python
import os
import sys
import json
import requests
from collections import Counter

import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import wordnet as wn
from nltk.stem import WordNetLemmatizer

from strip_tags import strip_tags

FILTERED_WORDS = {
    'var',
    '||',
    '&&',
    '=',
}
MIN_WORDS_PER_LINE = 20

def text_synset_similarity(text, comp_synset):
    words_sim = []
    #www = {}
    for lemma, synsets in gen_lemma_synsets(text):
        sims = [wn.path_similarity(comp_synset, ss) for ss in synsets]
        sims = [x for x in sims if x is not None]
        if not sims:
            continue
        # Take the most similar synset of the word to the comparison
        words_sim += [max(sims)]
        #www[str(lemma)] = max(sims)
    if not words_sim:
        return 0.0
    # Take the top five most similar words and average them
    words_sim = sorted(words_sim, reverse=True)[:5]
    #print(json.dumps([
    #    ['symset', str(comp_synset)],
    #    www
    #], indent=4))
    return sum(words_sim) / float(len(words_sim))

def tag_to_wn(tag):
    if tag.startswith('J'):
        return wn.ADJ
    elif tag.startswith('N'):
        return wn.NOUN
    elif tag.startswith('R'):
        return wn.ADV
    elif tag.startswith('V'):
        return wn.VERB
    return None

def has_blacklisted_words(line):
    ''' regarding javascript for the most part '''
    words = set(line.split())
    return words & FILTERED_WORDS

def get_text(url):
    content = requests.get(url).content.decode('ascii', 'replace').encode('ascii', 'replace')
    text = strip_tags(content)
    lines = [line.strip() for line in text.splitlines() if line.strip()]
    lines = [line for line in lines if not has_blacklisted_words(line)]
    lines = [line for line in lines if len(line.split()) > MIN_WORDS_PER_LINE]
    return '\n'.join(lines)

def gen_lemma_synsets(text):
    words = word_tokenize(text)
    lemmatzr = WordNetLemmatizer()
    for word, tag in nltk.pos_tag(words):
        pos = tag_to_wn(tag)
        if pos in (None, wn.VERB):
            continue
        lemma = lemmatzr.lemmatize(word, pos=pos)
        synsets = wn.synsets(lemma, pos=pos)
        if not synsets:
            continue
        yield lemma, synsets

def synset_counter(text):
    ctr = Counter()
    for lemma, synsets in gen_lemma_synsets(text):
        ctr.update(synsets)
    most_common = [(ss, ct) for ss, ct in ctr.most_common() if ct > 4]
    if not most_common:
        return ctr.most_common(5)
    return most_common

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('url')
    args = parser.parse_args()
    text = get_text(args.url)
    ctr = synset_counter(text)
    print(ctr)

if __name__ == '__main__':
    main()
