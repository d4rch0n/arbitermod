#!/usr/bin/env python
''' arbitermod.config

Logic to print and create configs, as well as export for other modules
'''
import os
import stat
import sys
import yaml
from yamlcfg import YamlConfig

DEFAULT_CONFIG_PATHS = (
    './arbitermod.yml',
    '~/.arbitermod.yml',
    '/etc/arbitermod/config.yml'
)

CONFIG = YamlConfig(paths=DEFAULT_CONFIG_PATHS)

def print_config():
    ''' pretty print the YAML config, or warn it doesn't exist '''
    if not CONFIG._data:
        print('No config found. Run:')
        print('$ arbitermod-config create')
        sys.exit(1)
    print(yaml.dump(CONFIG._data, default_flow_style=False))

def create():
    ''' create a config into the user's home dir '''
    from getpass import getpass
    username = raw_input('Username: ')
    password = getpass('Enter password')
    password2 = getpass('Confirm password')
    if not (password and password == password2):
        sys.exit('Password didn\'t match.')
    subreddit = raw_input('Default subreddit: ')
    default_config = {
        'username': username,
        'password': password,
        'subreddit': subreddit,
        'useragent': 'praw-arbitermod',
    }
    confpath = os.path.expanduser('~/.arbitermod.yml')
    with open(confpath, 'w') as f:
        yaml.dump(default_config, f, default_flow_style=False)
    # chmod to 600
    os.chmod(confpath, stat.S_IRUSR | stat.S_IWUSR)
    print('Configuration dumped to {}'.format(confpath))

def main():
    ''' two commands allowed: print, create '''
    import argparse
    parser = argparse.ArgumentParser()
    subs = parser.add_subparsers(dest='command')
    sub_create = subs.add_parser('create')
    sub_print = subs.add_parser('print')
    args = parser.parse_args()
    if args.command == 'create':
        create()
    elif args.command == 'print':
        print_config()

def warn():
    ''' Warn user if they're missing their config variables. '''
    if not all((CONFIG.username, CONFIG.password, CONFIG.subreddit)):
        sys.stderr.write('Config not found.\nYou can run `arbitermod-config '
            'create` to create a default configuration and place it in one of {},'
            ' loaded in that order.'.format(', '.join(DEFAULT_CONFIG_PATHS)))

if __name__ == '__main__':
    main()
