#!/usr/bin/env python

import sys
import os
import json
import re

from yamlcfg import YamlConfig

import praw
from praw.errors import RateLimitExceeded


CONF = YamlConfig(paths=('./config.yml', '~/.arbitermod.yml', '/etc/arbitermod.yml'))

RE_WAIT = re.compile(r'.*try again in (\d+) minute.*')
UA = CONF.useragent or 'praw-test 0.1'
SUB = CONF.subreddit or 'science'

def login():
    red = praw.Reddit(UA)
    red.login(username=CONF.username, password=CONF.password)
    return red

def crawl(red, sub_name=SUB, limit=None):
    sub = red.get_subreddit(sub_name)
    for comment in sub.get_comments(limit=limit):
        if hasattr(comment, 'body'):
            yield comment

red = login()
cs = list(crawl(red, limit=50))
