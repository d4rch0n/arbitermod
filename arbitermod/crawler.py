#!/usr/bin/env python

import sys
import os
import json
import re
import yaml

import praw
from praw.errors import RateLimitExceeded

from config import CONFIG

import judge
import analyzer

RE_WAIT = re.compile(r'.*try again in (?P<minutes_throttled>\d+) minute.*')
DEFAULT_UA = CONFIG.useragent or 'arbitermod-test'
DEFAULT_SUB = CONFIG.subreddit or 'science'


def login():
    red = praw.Reddit(DEFAULT_UA)
    if all((CONFIG.username, CONFIG.password)):
        red.login(username=CONFIG.username, password=CONFIG.password)
    else:
        red.login()
    return red

def flatten(comments):
    return praw.helpers.flatten_tree(comments)

def get_root_comments(red, submission):
    ''' Stupid hack to get root level comments.
    I could probably just skip the flatten step, but this works too, for sure.
    '''
    bodied = [x for x in flatten(submission.comments) if hasattr(x, 'body')]
    submission_parent_id = None
    for comment in bodied:
        if submission_parent_id is None:
            parent = red.get_info(thing_id=comment.parent_id)
            if isinstance(parent, praw.objects.Submission):
                submission_parent_id = comment.parent_id
                yield comment
        else:
            if comment.parent_id == submission_parent_id:
                yield comment

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--topic-threshold', '-t', type=float)
    parser.add_argument('--polar-threshold', '-p', type=float)
    parser.add_argument('--way-off-threshold', '-w', type=float)
    parser.add_argument('--comment-limit', '-l', type=int, default=0)
    parser.add_argument('--yaml-dump', '-y')
    parser.add_argument('--dry-run', '-D', action='store_true')
    parser.add_argument('--debug', '-d', action='store_true')
    parser.add_argument('comment_url')
    args = parser.parse_args()
    red = login()
    url = args.comment_url
    limit = args.comment_limit
    print('Retrieving submission')
    submission = red.get_submission(url)
    print('Retrieving submission URL text')
    text = analyzer.get_text(submission.url)
    print('Analyzign submission URL')
    ss_ctr = analyzer.synset_counter(text)
    datadump = {'REMOVED': [], 'KEPT': [], 'TOPIC_SCORE': [], 'POLAR_SCORE': []}
    for i, comment in enumerate(get_root_comments(red, submission)):
        if limit > 0 and i >= limit:
            break
        topic_score = judge.topic_score(comment, ss_ctr)
        polar_score = judge.polarity_score(comment)
        if topic_score is None or polar_score is None:
            continue
        len_score = judge.score_sentence_len(comment)
        body = comment.body.strip().replace('\n', '[...]')[:512].encode('ascii', 'replace')
        body2 = comment.body.strip().replace('\n\n', '\n').encode('ascii', 'replace')
        print('comment [{}, {}, {}]: {}'.format(topic_score, polar_score,
            len_score, body))
        failed = judge.scores_fail(topic_score, polar_score, len_score,
            topic_threshold=args.topic_threshold,
            polar_threshold=args.polar_threshold,
            way_off=args.way_off_threshold)
        if failed:
            datadump['REMOVED'] += [{
                'topic_score': topic_score, 
                'polar_score': polar_score, 
                'comment_body': body2
            }]
            print('***REMOVE***')
        else:
            datadump['KEPT'] += [{
                'topic_score': topic_score, 
                'polar_score': polar_score, 
                'comment_body': body2
            }]
            print('***KEEP***')
        datadump['TOPIC_SCORE'] += [[topic_score, body2]]
        datadump['POLAR_SCORE'] += [[polar_score, body2]]
    datadump['TOPIC_SCORE'] = sorted(datadump['TOPIC_SCORE'])
    datadump['POLAR_SCORE'] = sorted(datadump['POLAR_SCORE'])
    if args.yaml_dump:
        with open(args.yaml_dump, 'w') as f:
            yaml.dump(datadump, f, default_flow_style=False)

if __name__ == '__main__':
    main()
