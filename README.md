arbitermod
==========

Auto-moderates a subreddit (reddit.com forum) based on similarity of comment to
submission topic.

Setup:

```bash
$ sudo pip install arbitermod
# To obtain NLTK data, run this and download "all"
$ python -c "import nltk ; nltk.download()"
$ arbitermod-config create
# To verify config
$ arbitermod-config print
```

Example:

```bash
$ arbitermod http://www.reddit.com/r/science/comments/38pc1g/cat_parasite_toxoplasma_gondii_linked_to_mental/ 
```
